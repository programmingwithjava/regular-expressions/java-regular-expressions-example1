public class Main {
    public static void main(String[] args) {
        /*
        * the simplest form of a regular expression is a String literal.
        * for example "honor" is a regular expression.
        * */
        String myString = "I love Java. I love Spring. I love JavaScript.";
        /*
        * here the first argument is the regex to be matched, the second argument is the replacement.
        * */
        String yourString = myString.replaceAll("I", "You");
        System.out.println(yourString);

        /**
         * CHARACTER CLASSES AND BOUNDARY MATCHERS
         * character classes: represents a set or class of characters and
         * boundary matchers: looks for boundaries such as the beginning or end of a string or word
         * . character class will match any character.
         * when we use ^ boundary matcher, the regular expression must match the beginning of a string.
         */
        System.out.println(myString.replaceAll(".", "x"));

        /*
        * only the "ssR at the beginning will be matched and replaced"
        * */
        String testString = " \t6assRGvsj7b3 874bcdfssRDFssR9  \n";
        System.out.println(testString.replaceAll("^ssR", "x"));

        /*
        * string as a whole should match the regular expression in order to return true as result
        * therefore the output of the below line will be false.
        * */
        System.out.println(testString.matches("^ssR"));
        //below will print true
        System.out.println(testString.matches("^ssRGvsj73874bfssRDF"));

        /*
        * $ boundary matcher is the opposite of ^ boundary matcher and it is always preceded by a regular expression
        * */
        System.out.println(testString.replaceAll("ssR$", "END"));

        /*
        * when we wanna match a specific letter or a set of letters we can put them in square brackets
        * below, all the occurrences of 7 and s will be replaced by x
        * */
        System.out.println(testString.replaceAll("[7s]", "REPLACED"));
        /*
        * below a match will occur only if one of 7 or s is followed by R or 3 and that match will be replaced.
        * for example first sR will be replaced by "REPLACED"
        * */
        System.out.println(testString.replaceAll("[7s][R3]", "REPLACED"));

        /*
        * find all words whether it starts with upper or lower case and replace them
        * */
        System.out.println("serdar serdar Serdar".replaceAll("[sS]erdar", "Serdar"));

        /*
        * replace all letters except some
        * when we use ^ inside square brackets as the first character, it is actually a character class
        * not a boundary matcher. it negates the pattern that follows it.
        * below will match all the characters that are not s or j.
        * */
        System.out.println(testString.replaceAll("[^sj]", "X"));

        /*
        * match a range of characters
        * */
        System.out.println(testString);
        System.out.println(testString.replaceAll("[abcd678]", "X"));

        /*
        * dash is used to specify a range
        * */
        System.out.println(testString.replaceAll("[a-d6-8]", "X"));

        /*
        * also match the uppercase A-D
        * */
        System.out.println(testString.replaceAll("[a-dA-D6-8]", "X"));

        /*
        * turn off case sensitivity (?i) construct is used to achieve this
        * if string is unicode use (?iu) construct
        * */
        System.out.println(testString.replaceAll("(?i)[a-d6-8]", "X"));

        /*
        * replace all numbers
        * */
        System.out.println(testString.replaceAll("[0-9]", "X"));

        /*
        * \d can be used to achieve the same thing as above.
        * don't forget to escape \ character using \\
        * */
        System.out.println(testString.replaceAll("\\d", "X"));

        /*
        * replace all non-digits
        * */
        System.out.println(testString.replaceAll("\\D", "X"));

        /*
        * remove all whitespaces.
        * this remove all whitespace tab and newline.
        * if you want to remove specific whitespace characters you can specify those in a
        * regular expression like \t \n etc.
        * */
        System.out.println(testString.replaceAll("\\s", ""));

        /*remove only the tab character*/
        System.out.println(testString.replaceAll("\t", "X"));

        /*
        * replace all non-whitespace characters
        * */
        System.out.println(testString.replaceAll("\\S", "X"));

        /*
        * \w matches a-zA-Z0-9_
        * */
        System.out.println(testString.replaceAll("\\w", "X"));

        /*
        * opposite of the above match
        * */
        System.out.println(testString.replaceAll("\\W", "X"));
        System.out.println(testString);

        /*
        * \b is used to match word boundaries
        * below we surround words with | character
        * it assumes that words are seperated by whitespace.
        * */
        System.out.println(testString.replaceAll("\\b", "|"));
    }
}
